using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movimiento : MonoBehaviour
{

    public float velocidad;
    Vector3 dir;

    float step;
    Vector3 LastPos;
    Control control;
    // Cola

    bool growingPending;
    public GameObject tailPrefab;

    // Comida y Bomba
    public GameObject foodPrefab;
    public GameObject bombaPrefab;

    // Limites 
    public GameObject leftSide;
    public GameObject righSide;
    public GameObject upSide;
    public GameObject downSide;

    public List<Transform> tail = new List<Transform>();

    enum Control
    {

        Up,
        Down,
        Left,
        Right,

    }
    public void Start()
    {
        step = GetComponent<SpriteRenderer>().bounds.size.x;
        StartCoroutine(MoveCoroutine());
        CreateFood();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            control = Control.Right;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            control = Control.Left;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            control = Control.Up;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            control = Control.Down;
        }

    }


    void Move()
    {

        LastPos = transform.position;

        Vector3 nextPos = Vector3.zero;
        if (control == Control.Left)
        {
            nextPos = Vector3.left;
        }
        else if (control == Control.Right)
        {
            nextPos = Vector3.right;
        }
        else if (control == Control.Up)
        {
            nextPos = Vector3.up;
        }
        else if (control == Control.Down)
        {
            nextPos = Vector3.down;
        }

        transform.position += nextPos * step;

        MoveTail();
    }
    void MoveTail()
    {

        for (int i = 0; i < tail.Count; i++)
        {

            Vector3 temp = tail[i].position;
            tail[i].position = LastPos;
            LastPos = temp;
        }
        if (growingPending) CreateTail();
    }

    IEnumerator MoveCoroutine()
    {

        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            Move();
        }

    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Food" )
        {
            growingPending = true;
            Destroy(collision.gameObject);
            CreateFood();
            CreateBomba();
           
            
        }
        else if (collision.gameObject.tag == "Limite")
        {
            SceneManager.LoadScene("GameOver");
        }
        else if(collision.gameObject.tag == "Bomba")
        {
            SceneManager.LoadScene("GameOver");
        }

    }

    void CreateTail()
    {
        GameObject newTail = Instantiate(tailPrefab, LastPos, Quaternion.identity);
        newTail.name = "Tail_" + tail.Count;
        tail.Add(newTail.transform);
        growingPending = false;

    }
    public void CreateFood()
    {
        Vector2 pos = new Vector2(Random.Range(leftSide.transform.position.x, righSide.transform.position.x), Random.Range(upSide.transform.position.y, downSide.transform.position.y));
        Instantiate(foodPrefab, pos, Quaternion.identity);
        GameObject[] go = GameObject.FindGameObjectsWithTag("Bomba");


    }

    void CreateBomba()
    {
        Vector2 bomba = new Vector2(Random.Range(leftSide.transform.position.x, righSide.transform.position.x), Random.Range(upSide.transform.position.y, downSide.transform.position.y));

        Instantiate(bombaPrefab, bomba, Quaternion.identity);
    }
}